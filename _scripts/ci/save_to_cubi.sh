#!/bin/bash
#
# This file is part of SystemTestPortal.
# Copyright (C) 2018  Institute of Software Technology, University of Stuttgart
#
# SystemTestPortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SystemTestPortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
#

BASE_FOLDER="/cubitruck-volume"
RELEASE_FOLDER="releases"
# test if current commit is tagged then we have a release build
if git describe --exact-match --tags HEAD ; then
    # save build artifacts to a folder with the same name as the tag
    RELEASE_TAG=$(git describe --tags)
    TARGET_FOLDER="$BASE_FOLDER/$RELEASE_FOLDER/$RELEASE_TAG"

    # copy source to binary directory
    cp -a /builds/stp-team/systemtestportal-webapp/source/. /builds/stp-team/systemtesptortal-webap/binaries/
else
    # save build artifacts to nightly folder as they are from a regular non tagged nightly build
    DATE=`date +%Y%m%d`
    TARGET_FOLDER="$BASE_FOLDER/nightly/$DATE"

    # rename all files to have nightly build as part of the file name (replaces "systemtestportal" with "systemtestportal-nightly")
    for i in /builds/stp-team/systemtestportal-webapp/binaries/systemtestportal*; 
        do mv "$i" "${i/systemtestportal/systemtestportal-nightly}"; 
    done
fi

echo "The build files will be saved to:"
echo $TARGET_FOLDER

rm -rf ${TARGET_FOLDER}
mkdir -p ${TARGET_FOLDER} # recursive folder creation

ls -l $BASE_FOLDER/$RELEASE_FOLDER

# copy generated binaries from previous job into target folder on cubitruck
cp -a /builds/stp-team/systemtestportal-webapp/binaries/. ${TARGET_FOLDER}/

# ------------------
# set symlink from the latest folder to the most recent tagged build

# get the most recent tag from origin master
MOST_RECENT_TAG="$(git describe --tags --abbrev=0 --match v* `git rev-list --tags --max-count=1`)"

# if the current tag is equal to the most recent tag we add the symlink to latest
# if we rerun a tagged build pipline and it is not the most recent tag, the latest symlink will
# not be changed
if [ "$RELEASE_TAG" == "$MOST_RECENT_TAG" ]; then
    rm -rf $BASE_FOLDER/$RELEASE_FOLDER/latest # remove old link
    cd $BASE_FOLDER/$RELEASE_FOLDER
    ln -sfv ${RELEASE_TAG} latest
    echo "latest link:"
    echo $(readlink -- $BASE_FOLDER/$RELEASE_FOLDER/latest) # print symlink
fi

# -------------------------------------------------
# print saved files on cubi for debugg purposes
echo "cubitruck-volume:"
cd $BASE_FOLDER
pwd
ls -l

echo "parent folder of cubitruck-volume:"
cd ..
pwd
ls -l 

echo "releases folder:"
cd $BASE_FOLDER/$RELEASE_FOLDER
pwd
ls -l

echo "nightly folder"
cd $BASE_FOLDER/nightly
pwd
ls -l

echo "content of target folder"
cd ${TARGET_FOLDER}/
pwd
ls -l
