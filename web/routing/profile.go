/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */

package routing

import (
	"github.com/dimfeld/httptreemux"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/update"
)

func registerProfileHandler(r *httptreemux.ContextMux) {
	userStore := store.GetUserStore()

	r.GET(Users+VarProfile, display.ShowProfileGet(userStore))
	r.GET(Users+VarProfile+Settings, display.EditProfileGet(userStore))
	r.PUT(Users+VarProfile+Settings, update.EditProfilePut(userStore, userStore))
	r.PUT(Users+VarProfile+Settings+ChangePassword, update.EditPasswordPut(userStore, userStore, userStore))
	r.PUT(Users+VarProfile+Settings+DeactivateUser, update.DeactivateUserPut(userStore, userStore))
}
