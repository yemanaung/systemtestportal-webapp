/*
 * This file is part of SystemTestPortal.
 * Copyright (C) 2017-2018  Institute of Software Technology, University of Stuttgart
 *
 * SystemTestPortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SystemTestPortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
 */

package util

import (
	"testing"
)

func TestParseMarkdown(t *testing.T) {
	source := "**This** is a *markdown* test!"
	want := "<p class=\"markdown-paragraph\"><strong>This</strong> is a <em>markdown</em> test!</p>"
	is := ParseMarkdown(source)
	if is != want {
		t.Errorf("Parsed string doesn't match "+
			"expectation: %s, want %s", is, want)
	}
}

func TestSanitize(t *testing.T) {
	source := "<p class=\"test markdown-paragraph\">Test</p><strong>Test</strong><script>alert('XSS');<script>"
	want := "<p>Test</p><strong>Test</strong>"
	is := Sanitize(source)
	if is != want {
		t.Errorf("Sanitized string doesn't match "+
			"expectation: %s, want %s", is, want)
	}
}

func TestCutMarkdown(t *testing.T) {
	source := "**This** is a *markdown* test!"
	want := "This is a markdown test!"
	is := CutMarkdown(source)
	if is != want {
		t.Errorf("Cut string doesn't match "+
			"expectation: %s, want %s", is, want)
	}
}
func TestCutHTML(t *testing.T) {
	source := "<strong>Test</strong><script>alert('XSS');<script>"
	want := "Test"
	is := CutHTML(source)
	if is != want {
		t.Errorf("Cut string doesn't match "+
			"expectation: %s, want %s", is, want)
	}
}