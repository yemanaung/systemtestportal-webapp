/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"bytes"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/contextdomain"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// CaseCommentPut returns a function
// which processes a comment posted to a test case
func CaseCommentPut(commentAdder handler.CommentAdder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.User == nil || c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		text := r.FormValue(httputil.CommentText)

		newComment := &comment.Comment{Text: text, Author: c.User, Requester: c.User, AuthorRole: c.Project.GetRole(c.User)}
		err := commentAdder.InsertComment(newComment, *c.Case, c.User)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		err = WriteComment(newComment, w, r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}

// SequenceCommentPut returns a function
// which processes a comment posted to a test sequence
func SequenceCommentPut(commentAdder handler.CommentAdder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.User == nil || c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		text := r.FormValue(httputil.CommentText)

		newComment := &comment.Comment{Text: text, Author: c.User, Requester: c.User, AuthorRole: c.Project.GetRole(c.User)}

		err := commentAdder.InsertComment(newComment, *c.Sequence, c.User)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		err = WriteComment(newComment, w, r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}

}

/*
Write comment renders the given comment and prints it into the given Writer.
*/
func WriteComment(c *comment.Comment, w http.ResponseWriter, r *http.Request) error {
	tmpl := handler.GetBaseTree(r).Append(templates.Comment).Get().Lookup("comment")

	b := &bytes.Buffer{}
	data, err := handler.Dict("Comment", c, "SystemSettings", contextdomain.GetGlobalSystemSettings())
	if err != nil {
		return err
	}
	if err := tmpl.Execute(b, data); err != nil {
		return err
	}

	if _, err := b.WriteTo(w); err == nil {
		return err
	}

	return nil
}
