/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"net/http"
	"strconv"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/system"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

func SystemSettingsPut(systemSettingsStore handler.SystemSettings) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		var globalMessageExpirationDate time.Time
		insertRequired := false
		invalidFields := []string{}

		// Get the information from the request

		// MESSAGE ----------------------------------------------------------------------------------------------------|

		globalMessage := r.FormValue(httputil.GlobalMessage)
		if len(globalMessage) > 250 {
			invalidFields = append(invalidFields, "GlobalMessage is too long")
		}

		globalMessageType := r.FormValue(httputil.GlobalMessageType)

		// BOOLEANS ---------------------------------------------------------------------------------------------------|

		isDisplayGlobalMessage, err := strconv.ParseBool(r.FormValue(httputil.IsDisplayMessage))
		if err != nil {
			invalidFields = append(invalidFields, "DisplayGlobalMessage is not a boolean")
		}

		// If the message is empty don't show the message
		if globalMessage == "" {
			isDisplayGlobalMessage = false
		}

		isGlobalMessageExpirationDate, err := strconv.ParseBool(r.FormValue(httputil.IsGlobalMessageExpirationDate))
		if err != nil {
			invalidFields = append(invalidFields, "UseExpirationDate is not a boolean")
		}

		isAccessAllowed, err := strconv.ParseBool(r.FormValue(httputil.IsAccessAllowed))
		if err != nil {
			invalidFields = append(invalidFields, "AccessAllowed is not a boolean")
		}

		isRegistrationAllowed, err := strconv.ParseBool(r.FormValue(httputil.IsRegistrationAllowed))
		if err != nil {
			invalidFields = append(invalidFields, "RegistrationAllowed is not a boolean")
		}

		isEmailVerfication, err := strconv.ParseBool(r.FormValue(httputil.IsEmailVerification))
		if err != nil {
			invalidFields = append(invalidFields, "EmailVerification is not a boolean")
		}
		isDeleteUsers, err := strconv.ParseBool(r.FormValue(httputil.IsDeleteUsers))
		if err != nil {
			invalidFields = append(invalidFields, "DeleteUsers is not a boolean")
		}
		isExecutionTime, err := strconv.ParseBool(r.FormValue(httputil.IsExecutionTime))
		if err != nil {
			invalidFields = append(invalidFields, "ExecutionTime is not a boolean")
		}
		// TIME -------------------------------------------------------------------------------------------------------|

		globalMessageTimeStamp := r.FormValue(httputil.GlobalMessageTimeStamp)
		globalMessageExpirationDateUnix := r.FormValue(httputil.GlobalMessageExpirationDateUnix)

		// Imprint and Privacy ----------------------------------------------------------------------------------------|

		imprintMessage := r.FormValue(httputil.ImprintMessage)
		privacyMessage := r.FormValue(httputil.PrivacyMessage)

		// Check if the date is valid
		globalMessageExpirationDateString := r.FormValue(httputil.GlobalMessageExpirationDate)
		if globalMessageExpirationDateString != "" {
			// But only if the field wasn't empty
			globalMessageExpirationDate, err = time.Parse(system.DatePickerLayout, globalMessageExpirationDateString)
			if err != nil {
				invalidFields = append(invalidFields, "ExpirationDate is not a valid date")
			}
		} else {
			isGlobalMessageExpirationDate = false
		}

		//Check if all Data vas valid; Send Response Code 400 if not + Details in the Response Text
		if len(invalidFields) > 0 {
			var responseText string

			for i := 0; i < len(invalidFields); i++ {
				if i == 0 {
					responseText = " - " + invalidFields[i]
				} else {
					responseText = responseText + "\n" + " - " + invalidFields[i]
				}
			}

			w.WriteHeader(400)
			w.Write([]byte(responseText))
			//return
		} else {

			// Get the systemsettings from storage
			systemSettings, err := systemSettingsStore.GetSystemSettings()
			if err != nil {
				errors.Handle(err, w, r)
				return
			}

			// Check if we even already have system settings
			if systemSettings.Id == 0 {
				insertRequired = true
			}

			// Fill the systemsettings
			systemSettings.IsDisplayGlobalMessage = isDisplayGlobalMessage
			systemSettings.IsAccessAllowed = isAccessAllowed
			systemSettings.IsRegistrationAllowed = isRegistrationAllowed
			systemSettings.IsEmailVerification = isEmailVerfication
			systemSettings.IsDeleteUsers = isDeleteUsers
			systemSettings.IsGlobalMessageExpirationDate = isGlobalMessageExpirationDate
			systemSettings.IsExecutionTime = isExecutionTime
			systemSettings.GlobalMessage = globalMessage
			systemSettings.GlobalMessageTimeStamp = globalMessageTimeStamp
			systemSettings.GlobalMessageExpirationDate = globalMessageExpirationDate
			systemSettings.GlobalMessageExpirationDateUnix = globalMessageExpirationDateUnix
			systemSettings.ImprintMessage = imprintMessage
			systemSettings.PrivacyMessage = privacyMessage

			if globalMessageType == "" {
				globalMessageType = "system-message-notification"
			}
			systemSettings.GlobalMessageType = globalMessageType

			if insertRequired {
				err := systemSettingsStore.InsertSystemSettings(systemSettings)
				if err != nil {
					errors.Handle(err, w, r)
					return
				}
			} else {
				err := systemSettingsStore.UpdateSystemSettings(systemSettings)
				if err != nil {
					errors.Handle(err, w, r)
					return
				}
			}
		}
	}
}
