/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/dashboard"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/export"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
	"html/template"
	"net/http"
)

// ProjectsGet serves the page that is used to explore projects.
func GlobalDashboardGet(projectLister handler.ProjectLister, testCaseLister handler.TestCaseLister, caseProtocolLister handler.CaseProtocolLister) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {

		var projectList []*project.Project
		var projectCountMap = make(map[*project.Project][6]float64)
		var counter [6] float64

		contextEntities := handler.GetContextEntities(request)

		// If no user is logged in, only show public projects
		if contextEntities.User == nil {
			publicProjects, err := projectLister.ListPublic()
			if err != nil {
				errors.Handle(err, writer, request)
				return
			}
			projectList = append(projectList, publicProjects...)
		} else {
			// If user is logged-in, show public, internal and the
			// private projects the logged-in user has access to
			actorProjects, err := projectLister.ListForActor(contextEntities.User.ID())
			if err != nil {
				errors.Handle(err, writer, request)
				return
			}
			projectList = append(projectList, actorProjects...)
		}

			for j := range projectList {
				var versions []*project.Version
				var lastversion *project.Version

				counter = [6] float64{0, 0, 0, 0, 0, 0}

				// number of testcases
				counter[0] = float64(len(export.GetCases(testCaseLister, projectList[j])))

				// get protocols for the project
				caseProtocolList := display.GetCaseProtocols(projectList[j], writer, request)
				caseProtocolMap := display.GetProtocolMap(caseProtocolList, caseProtocolLister)

				for _, version := range projectList[j].Versions {
					versions = append(versions, version)
				}
				dashboard.SortVersions(versions)
				for _, version := range versions {

 					lastversion = version
				}

				for k := range caseProtocolMap {

					for i := 0; i < len(caseProtocolMap[k]); i++ {

						// latest version
						if (caseProtocolMap[k][i].SUTVersion == lastversion.Name) {

							switch caseProtocolMap[k][i].Result.String() {
							case "Pass":
								counter[1]++
							case "PartiallySuccessful":
								counter[2]++
							case "Fail":
								counter[3]++
							case "NotAssessed":
								counter[4]++
							}

						}

					}

				}

				// calculate not yet executed testcases
				counter[5] = counter[0] - counter[1] - counter[2] - counter[3] - counter[4]

				// save count for the project
				projectCountMap[projectList[j]] = counter
			}

		tmpl := getExploreGlobalDashboard(request)
		handler.PrintTmpl(context.New().
			WithUserInformation(request).
			With(context.Projects, projectCountMap), tmpl, writer, request)
	}
}

func getExploreGlobalDashboard(r *http.Request) *template.Template {
	return handler.GetSideBarTree(r).

		Append(templates.ExploreGlobalDashboard).
		Get().Lookup(templates.HeaderDef)
}
