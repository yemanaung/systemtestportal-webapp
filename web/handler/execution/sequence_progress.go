/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

// sequenceProgress is a progressMeter used to show
// progress during the execution of sequence.
type sequenceProgress struct {
	caseProgress
}

// Init initializes the progress meter with given request data.
func (p *sequenceProgress) Init(r *http.Request, previous bool) error {
	c := handler.GetContextEntities(r)
	if c.Sequence == nil {
		return c.Err
	}

	tsv, err := handler.GetTestSequenceVersion(r, c.Sequence)
	if err != nil {
		return err
	}
	stepNr := getFormValueInt(r, keyStepNr)
	caseNr := getFormValueInt(r, keyCaseNr)

	if previous {
		p.cur = totalWorkStepsUpTo(*tsv, caseNr-1) + stepNr + 1
	} else {
		p.cur = totalWorkStepsUpTo(*tsv, caseNr-1) + stepNr + 2
	}
	p.max = totalWorkSteps(*tsv)

	return nil
}
