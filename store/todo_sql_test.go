// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

package store

import (
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
)

func TestGetAssigneesForTestCaseMultiple(t *testing.T) {
	tasks := TaskSQL{engineWithSampleData(t)}

	assignees, err := tasks.GetTasksForTest(id.NewTestID(id.NewProjectID(id.NewActorID("theo"), "GoUnit"), "Simple test suite", true))

	if err != nil {
		t.Errorf("Unexpected error occured while retrieving assignees:\n%s", err)
	}

	if len(assignees) != 2 {
		t.Errorf("List of assignees does not match expected result:\n%v", assignees)
	}
}

func TestGetAssigneesForTestSequenceSingle(t *testing.T) {
	tasks := TaskSQL{engineWithSampleData(t)}

	assignees, err := tasks.GetTasksForTest(id.NewTestID(id.NewProjectID(id.NewActorID("theo"), "GoUnit"), "All kinds of test suite", false))

	if err != nil {
		t.Errorf("Unexpected error occured while retrieving assignees:\n%s", err)
	}

	if len(assignees) != 1 {
		t.Errorf("List of assignees does not match expected result:\n%v", assignees)
	}
}
